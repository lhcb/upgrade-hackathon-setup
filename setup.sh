#!/bin/bash
export CCACHE_DIR=${PWD}/.ccache
#export CCACHE_LOGFILE=$(dirname $(mktemp -u))/ccache.debug
export CMAKE_PREFIX_PATH=${PWD}:${CMAKE_PREFIX_PATH}
# Declare the top directory as current workspace (special treatment in the toolchain)
export LBENV_CURRENT_WORKSPACE=${PWD}
unset VERBOSE

# Tweaks for LXPLUS
if [[ $(hostname) == lxplus* ]]; then
  # use the faster TMPDIR
  export CCACHE_TEMPDIR=${TMPDIR-/tmp/$USER}/ccache
  mkdir -p ${CCACHE_TEMPDIR}
  # don't be too aggressive or else g++ gets killed
  export BUILDFLAGS=-j6
fi
