# Configuration for LHCb stack build
# ===================================================================
#
# The known variables are:
# - PROJECTS:
#     A list of projects to build.
#     If a branch (or tag) is optionally specified with Project/branch, it
#     will override the DEFAULT_BRANCH and Project_BRANCH. For example:
#         PROJECTS = Gaudi/v28r1 LHCb Lbcom Rec ...
# - DEFAULT_BRANCH:
#     Default branch to checkout for all projects.
# - Project_BRANCH:
#     branch/tag to checkout for Project, overrides DEFAULT_BRANCH
# - GIT_BASE:
#     git repository base URL, default is https://gitlab.cern.ch
#     If you change this, call "make set-git-remote-url" to apply the change
# - Project_GITGROUP:
#     GitLab group to use for finding the repository.
#     The URL is $(GIT_BASE)/$(Project_GITGROUP)/Project.git
# - Project_URL:
#     Override the default URL based on GIT_BASE and Project_GITGROUP.
# - Project_EXTRA_DEPS:
#     Project dependencies that are not automatically detected. If a project
#     is not in PROJECTS, the variable is ignored.
# - CMAKEFLAGS:
#     optional flags to pass to the configuration of all projects
# - Project_CMAKEFLAGS:
#     optional flags to pass to the configuration of a project (in addition
#     to the global ones)

# FIXME: this should be replaced with something like "lb-best-platform`
platform = x86_64_v2-el9-gcc13-dbg
# FIXME: this is only needed/used for new style CMake projects
LCG_VERSION = 105a

# Setting these to non empty values enable local builds of GitCondDB and DD4hep
WITH_GITCONDDB =
WITH_DD4HEP =

CMAKEFLAGS = -DPKG_CONFIG_USE_CMAKE_PREFIX_PATH=ON -DGAUDI_DIAGNOSTICS_COLOR=ON -DCMAKE_CXX_COMPILER_LAUNCHER:STRING=ccache
CMAKEFLAGS += -DGAUDI_LEGACY_CMAKE_SUPPORT=ON
# disable conflicting ccache prefix from old toolchain
CMAKEFLAGS += -DCMAKE_USE_CCACHE=OFF

PROJECTS = Gaudi Detector LHCb Lbcom Rec Brunel Hlt Stripping Analysis DaVinci Allen Moore Boole LHCbIntegrationTests Panoramix Vetra Online Alignment MooreOnline Geant4 Gauss Erasmus Urania Castelao Panoptes Run2Support testscope Gaussino GaussinoExtLibs
DEFAULT_BRANCH = master
# GIT_BASE = ssh://git@gitlab.cern.ch:7999

Gaudi_GITGROUP = gaudi
#Gaudi_BRANCH = master

Gaussino_GITGROUP = Gaussino
GaussinoExtLibs_GITGROUP = Gaussino

ifneq ($(WITH_GITCONDDB),)
GitCondDB_CMAKEFLAGS = -DCATCH2_SOURCES_URL=https://lhcb-repository.web.cern.ch/repository/lhcb-contrib/Catch2/Catch2-3.0.0-preview1.tar.gz
PROJECTS += GitCondDB
Detector_EXTRA_DEPS += GitCondDB
endif

ifneq ($(WITH_DD4HEP),)
Detector_EXTRA_DEPS += DD4hep

## DD4hep support
DD4hep_URL = https://github.com/AIDASoft/DD4hep.git
DD4hep_BRANCH = master

DD4hep/CMakeLists.txt:
	@test -e DD4hep || ( git clone $(DD4hep_URL) DD4hep && cd DD4hep && git checkout $(DD4hep_BRANCH) )

DD4hep/build.$(platform)/build.ninja: DD4hep/CMakeLists.txt
	@(. /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_$(LCG_VERSION) $(subst _v3,,$(subst _v2,,$(platform))) && . `pwd`/setup.sh \
	  && cmake -S DD4hep -B DD4hep/build.$(platform) -GNinja \
	     -DCMAKE_INSTALL_PREFIX=`pwd`/DD4hep/InstallArea/$(platform) -DCMAKE_CXX_COMPILER_LAUNCHER:STRING=ccache -DDD4HEP_USE_GEANT4_UNITS=ON -DDD4HEP_SET_RPATH=NO -DCMAKE_BUILD_TYPE=$(if $(findstring opt,$(platform)),Release,Debug))

DD4hep/purge:
	$(RM) -r DD4hep/build.$(platform) DD4hep/InstallArea/$(platform)

DD4hep/%: $(DD4hep_DEPS) $(DD4hep_EXTRA_DEPS) DD4hep/build.$(platform)/build.ninja setup.sh
	@(. /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_$(LCG_VERSION) $(subst _v3,,$(subst _v2,,$(platform))) && . `pwd`/setup.sh \
	  && cmake --build DD4hep/build.$(platform) --target $*)

DD4hep: DD4hep/install

ALL_TARGETS += DD4hep DD4hep/purge DD4hep/install

purge: DD4hep/purge
remove-DD4hep:
	$(RM) -r DD4hep

deep-purge: remove-DD4hep
endif
