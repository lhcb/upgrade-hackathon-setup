# settings
include configuration.mk
-include .projects_deps.mk

SHELL=/bin/bash

# default target
DEFAULT_PROJECT=Brunel
default: $(DEFAULT_PROJECT)

CCACHE_DIR := $(shell . `pwd`/setup.sh ; echo $${CCACHE_DIR})

GIT_BASE := $(or $(GIT_BASE),https://gitlab.cern.ch)

# separate branch (or tag) from project/branch
project = $(firstword $(subst /, ,$1))
branch = $(or $(word 2,$(subst /, ,$1)),$(value 2))
$(foreach p,$(PROJECTS),$(eval $(call project,$(p))_BRANCH := $(call branch,$(p),$($(p)_BRANCH))))
PROJECTS := $(foreach p,$(PROJECTS),$(call project,$(p)))

# Make sure BINARY_TAG and CMTCONFIG are set and consistent.
# If one is not set, take the value from the other.
# If none is set, use the default platform from `configuration.mk`.
ifeq ($(BINARY_TAG)$(CMTCONFIG),)
  ifeq ($(CMTCONFIG),)
    BINARY_TAG := $(platform)
  else
    BINARY_TAG := $(CMTCONFIG)
  endif
  export BINARY_TAG := $(BINARY_TAG)
endif
ifeq ($(CMTCONFIG),)
  CMTCONFIG := $(BINARY_TAG)
  export CMTCONFIG := $(CMTCONFIG)
endif
ifneq ($(BINARY_TAG),$(CMTCONFIG))
  $(error Invalid environment: inconsistent values for BINARY_TAG and CMTCONFIG)
endif
# Make sure platform is consistent with BINARY_TAG
platform = $(BINARY_TAG)

# Make sure LCG_VERSION is propagated as an environment variable
export LCG_VERSION := $(LCG_VERSION)

# main targets
all: build
.git-setup.stamp:
	@if [ -z $$(git config --global user.email) ] ; then \
		echo "warning: setting dummy user.email for Git to noreply-lhcb-upgrade-hackathon@cern.ch" ; \
		git config --global user.email "noreply-lhcb-upgrade-hackathon@cern.ch" ; \
	fi
	@if [ -z $$(git config --global user.name) ] ; then \
		echo "warning: setting dummy user.name for Git to $$USER" ; \
		git config --global user.name "$$USER" ; \
	fi
	@touch $@

checkout: $(patsubst %,%-checkout,$(PROJECTS))
	@echo "checkout completed"
update: $(patsubst %,%-update,$(PROJECTS))
	@echo "update completed"
reset: $(patsubst %,%-reset,$(PROJECTS))
	@echo "reset completed"

use-git-https:
	@$(MAKE) set-git-remote-url GIT_BASE=https://gitlab.cern.ch
use-git-ssh:
	@$(MAKE) set-git-remote-url GIT_BASE=ssh://git@gitlab.cern.ch:7999
use-git-krb5:
	@$(MAKE) set-git-remote-url GIT_BASE=https://:@gitlab.cern.ch:8443

CMD = true
for-each:
	@for p in $(PROJECTS) ; do if [ -d $$p ] ; then ( cd $$p && pwd && $(CMD) ) ; fi ; done

build: $(PROJECTS)
clean: $(patsubst %,%-clean,$(PROJECTS))
purge: $(patsubst %,%-purge,$(PROJECTS))
deep-purge:
	$(RM) -r $(PROJECTS) .projects_deps.mk toolchain.cmake

help:
	@for t in $(ALL_TARGETS) ; do echo .. $$t ; done

# public targets: main targets
ALL_TARGETS = all build checkout update clean purge deep-purge use-git-https use-git-ssh use-git-krb5

# distribution
DIST_TAG := $(or $(DIST_TAG),$(shell git describe --match "*" --abbrev=0 --tags 2> /dev/null))
ifneq ($(DIST_TAG),)
PRE_BUILT_IMAGE := $(DIST_TAG).$(CMTCONFIG).tar.gz
$(PRE_BUILT_IMAGE):
	$(MAKE) build
	$(MAKE) build
	tar -c -z -f $@ .ccache $(PROJECTS)
dist: $(PRE_BUILT_IMAGE)
# copy image to /eos/project/l/lhcbwebsites/www/lhcbproject/dist
pull-build: .git-setup.stamp
	curl http://lhcbproject.web.cern.ch/lhcbproject/dist/$(PRE_BUILT_IMAGE) | tar -x -z -f -
endif

# ----------------------
# implementation details
# ----------------------
# remove unsatisfiable dependencies
$(foreach p,$(PROJECTS),$(eval $(p)_DEPS := $(filter $($(p)_DEPS), $(PROJECTS))))
# compute inverse deps for "clean" targets
$(foreach p,$(PROJECTS),$(foreach d,$($(p)_DEPS),$(eval $(d)_INV_DEPS += $(p))))
# public targets: project targets
ALL_TARGETS += $(foreach p,$(PROJECTS),$(p) $(p)-checkout $(p)-update $(p)-clean $(p)-purge fast/$(p) fast/$(p)-clean)

toolchain.cmake:
	@lb-project-init
	@echo 'file(GLOB local_projects LIST_DIRECTORIES true $(shell pwd)/*/InstallArea/$$ENV{BINARY_TAG})' >> toolchain.cmake
	@echo 'list(PREPEND CMAKE_PREFIX_PATH $${local_projects})' >> toolchain.cmake

define PROJECT_settings
# project settings
$(1)_GITGROUP := $$(or $$($(1)_GITGROUP),lhcb)
$(1)_URL := $$(or $$($(1)_URL),$(GIT_BASE)/$$($(1)_GITGROUP)/$(1).git)
$(1)_BRANCH := $$(or $$($(1)_BRANCH),$(DEFAULT_BRANCH))
# checkout/update
$(1)-checkout: toolchain.cmake
	@test -e $(1) || ( git clone --no-checkout $$($(1)_URL) $(1) && cd $(1) && git checkout $$($(1)_BRANCH) )
	@if [ -e $(1)/.gitmodules ] ; then ( cd $(1) && git submodule update --init --recursive || true ) ; fi
	@test -e $(1)/toolchain.cmake || ln -s ../toolchain.cmake $(1)
	@cd $(1) && lb-project-init
	@grep -Fxq "toolchain.cmake" $(1)/.git/info/exclude || echo "toolchain.cmake" >> $(1)/.git/info/exclude || true
	@test -h $(1)/run -o -e $(1)/run || (\
		echo -e '#!/bin/bash\n$$$$(dirname "$$$${BASH_SOURCE[0]}")/build.$$$${CMTCONFIG}/run "$$$$@"' > $(1)/run && \
		chmod +x $(1)/run)
	@grep -Fxq "run" $(1)/.git/info/exclude || echo "run" >> $(1)/.git/info/exclude || true

$(1)-update:
	-@cd $(1) && git pull origin $$($(1)_BRANCH)
$(1)-reset:
	-@cd $(1) && git fetch && git reset --hard origin/$$($(1)_BRANCH)

# generic build target
$(1)/%: $$($(1)_DEPS) $$($(1)_EXTRA_DEPS) $(1)-checkout setup.sh
	@(. `pwd`/setup.sh ; export CMAKEFLAGS="$$(CMAKEFLAGS) $$($(1)_CMAKEFLAGS)" ; (set -v ; $$(MAKE) -C $(1) $$*))
fast/$(1)/%: $(1)-checkout setup.sh
	@(. `pwd`/setup.sh ; export CMAKEFLAGS="$$(CMAKEFLAGS) $$($(1)_CMAKEFLAGS)" ; (set -v ; $$(MAKE) -C $(1) $$*))
# exception for Project/purge: always do fast/Project/purge
$(1)/purge: fast/$(1)/purge
# build... delegate to generic target
$(1): $(1)/install
fast/$(1): fast/$(1)/install
# clean
$(1)-clean: $(patsubst %,%-clean,$($(1)_INV_DEPS))
	$$(MAKE) fast/$(1)-clean
fast/$(1)-clean:
	-test -d $(1)/build.$$(CMTCONFIG) && $$(MAKE) fast/$(1)/clean
	$(RM) -r $(1)/InstallArea/$$(CMTCONFIG)
# purge
$(1)-purge:
	-test -e $(1) && $$(MAKE) fast/$(1)/purge
endef
$(foreach proj,$(PROJECTS),$(eval $(call PROJECT_settings,$(proj))))

ifeq ($(MAKECMDGOALS),)
  current_projects := $(DEFAULT_PROJECT)
else
  current_projects := $(sort $(shell echo $(MAKECMDGOALS) | sed -nE 's/\S+-\S+//g;s|fast/\S+||g;s|([^/]+)(/\S+)?|\1|g;s/default/$(DEFAULT_PROJECT)/g;p'))
endif
ifneq ($(current_projects),)
.projects_deps.mk: $(foreach p,$(current_projects),$(p)-checkout $(foreach d,$(value $(p)_DEPS),$(d)-checkout))
	@echo "computing projects dependencies for" $(sort $(subst -checkout,,$^))
	@if [ ! -e $@ ] ; then touch $@ ; fi
	@for c in */CMakeLists.txt ; do \
		echo $${c/\/CMakeLists.txt/}_DEPS = \
                        $$( if [ -e $${c/\/CMakeLists.txt/}/lhcbproject.yml ] ; then python -c 'import sys, yaml; print(*("{0} $$({0}_DEPS)".format(d) for d in yaml.safe_load(sys.stdin).get("dependencies", [])))' < $${c/\/CMakeLists.txt/}/lhcbproject.yml ; else cat $$c | sed 's/#.*//' | tr \\n ' ' | sed -nE '/\bUSE\b/{s/\s+/ /g;s/(v[0-9]+r[0-9]+[^ \t)]*|HEAD)/ /g;s/^.*gaudi_project\([^)]+\sUSE\s([^)]*)(\s+DATA.*|\s+\).*)/\1/;s/(\S+)/\1 $$(\1_DEPS)/g;p}' ; fi ) >> $@.tmp ; \
		done ; if diff -q $@.tmp $@ >/dev/null ; then $(RM) $@.tmp ; else mv -f $@.tmp $@ ; fi
endif

CCACHE := $(shell . `pwd`/setup.sh ; which ccache 2> /dev/null)
ifeq ($(CCACHE),)
  CCACHE := $(shell which ccache-swig 2> /dev/null)
endif
ifeq ($(CCACHE),)
  CCACHE := $(shell lb-run --ext ccache LCG/latest which ccache)
endif
ifneq ($(CCACHE),)
$(CCACHE_DIR):
	$(CCACHE) -F 20000 -M 0
$(PROJECTS): $(CCACHE_DIR)
endif

set-git-remote-url:
	@$(foreach p,$(PROJECTS),if [ -d $p ] ; then ( cd $p && pwd && git remote set-url origin $(GIT_BASE)/$($p_GITGROUP)/$p.git && git remote -v ) ; fi ;)

.PHONY: $(ALL_TARGETS) dist pull-build
